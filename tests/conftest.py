from datetime import datetime, timedelta

import pytest

from main.app import create_app
from main.app import db as _db
from main.models import Client, ClientParking, Parking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        client = Client(
            id=1,
            name="test_name",
            surname="test_surname",
            credit_card=True,
            car_number="test_car_number",
        )
        parking = Parking(
            id=1,
            address="test_address",
            opened=True,
            count_places=500,
            count_available_places=101,
        )
        client_parking = ClientParking(
            id=1,
            client_id=1,
            parking_id=1,
            time_in=datetime.now(),
            time_out=datetime.now() + timedelta(hours=2),
        )
        _db.session.add(client)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
