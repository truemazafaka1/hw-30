import pytest

from main.models import Client, ClientParking, Parking


def test_create_parking_handler(client) -> None:
    parking_data = {
        "address": "address",
        "opened": True,
        "count_places": 100,
        "count_available_places": 85,
    }

    resp = client.post("/parkings", data=parking_data)

    assert resp.status_code == 201


def test_create_client_handler(client) -> None:
    client_data = {
        "name": "test_name",
        "surname": "test_surname",
        "credit_card": True,
        "car_number": "test_car_number",
    }

    resp = client.post("/clients", data=client_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_client_parkings_handler(client, db) -> None:
    client_parking_data = {"client_id": 1, "parking_id": 1}

    resp = client.post("/client_parkings", data=client_parking_data)
    parking_check: Parking = db.session.query(Parking).get(1)
    parking_check.count_available_places -= 1

    assert resp.status_code == 308
    assert parking_check.opened is True
    assert parking_check.count_available_places == 100


@pytest.mark.parking
def test_client_leaving_handler(client, db) -> None:
    client_parking_data = {"client_id": 1, "parking_id": 1}

    resp = client.delete("/client_parkings", data=client_parking_data)
    client_park_check: ClientParking = db.session.query(ClientParking).get(1)
    client_check: Client = db.session.query(Client).get(1)
    parking_check: Parking = db.session.query(Parking).get(1)
    parking_check.count_available_places += 1
    assert resp.status_code == 308
    assert client_park_check.time_out > client_park_check.time_in
    assert client_check.credit_card is True
    assert parking_check.count_available_places == 102


def test_app_config(app):
    assert not app.config["DEBUG"]
    assert app.config["TESTING"]
    assert app.config["SQLALCHEMY_DATABASE_URI"] == "sqlite://"


@pytest.mark.parametrize("route", ["/clients", "/clients/1"])
def test_route_status(client, route):
    test_status_code = client.get(route)
    assert test_status_code.status_code == 200
