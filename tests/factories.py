import factory
import factory.fuzzy

from main.app import db
from main.models import Client, Parking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name: factory.Faker = factory.Faker("first_name")
    surname: factory.Faker = factory.Faker("last_name")
    car_number: factory.Faker = factory.Faker("ssn")
    credit_card: factory.Faker = factory.Faker("pybool")


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address: factory.Faker = factory.Faker("address")
    opened: factory.Faker = factory.Faker("pybool")
    count_places: factory.fuzzy.FuzzyInteger = factory.fuzzy.FuzzyInteger(
        50,
        500,
    )
    count_available_places: factory.LazyAttribute = factory.LazyAttribute(
        lambda o: o.count_places - 15,
    )
