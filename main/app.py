from datetime import datetime
from typing import Any, List

import sqlalchemy
from flask import Flask, Response, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///homework.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    @app.before_first_request
    def before_request_func():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["POST"])
    def create_client_handler():
        """Создание нового клиента"""
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        if credit_card == "True":
            credit_card = True
        elif credit_card == "False":
            credit_card = False
        car_number = request.form.get("car_number", type=str)

        new_client = Client(
            name=name,
            surname=surname,
            credit_card=credit_card,
            car_number=car_number,
        )

        db.session.add(new_client)
        db.session.commit()

        return "Client successfully added", 201

    @app.route("/clients", methods=["GET"])
    def get_client_handler() -> tuple[Response | Any, int]:
        """Получение клиентов"""
        clients: List[Client] = db.session.query(Client).all()
        clients_list = [c.to_json() for c in clients]
        return jsonify(clients_list), 200

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client_by_id_handler(client_id: int) -> tuple[Response | Any, int]:
        """Получение пользователя по id"""
        client: Client = db.session.query(Client).get(client_id)
        return jsonify(client.to_json()), 200

    @app.route("/parkings", methods=["POST"])
    def create_parking_handler():
        """Создание новой парковочной зоны"""
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=str)
        if opened == "True":
            opened = True
        elif opened == "False":
            opened = False
        count_places = request.form.get("count_places", type=int)
        c_a_p = request.form.get("count_available_places", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=c_a_p,
        )

        db.session.add(new_parking)
        db.session.commit()

        return "Parking successfully added", 201

    @app.route("/client_parkings/", methods=["POST"])
    def client_parkings_handler() -> tuple[str, int]:
        """Заезд клиента на парковочную зону"""

        try:
            client_id = request.form.get("client_id", type=int)
            parking_id = request.form.get("parking_id", type=int)
            parking_check: Parking = db.session.query(Parking).get(parking_id)
            if (
                parking_check.opened is True
                and parking_check.count_available_places > 0
            ):
                time_in = datetime.now()
                new_client_parking = ClientParking(
                    client_id=client_id, parking_id=parking_id, time_in=time_in
                )
                parking_check.count_available_places -= 1
                db.session.add(new_client_parking)
                db.session.commit()
                return (
                    f"Client: {client_id} parked successfully"
                    f" on parking zone: {parking_id}",
                    308,
                )
            else:
                return (
                    f"Parking is not possible."
                    f" Parking: {parking_id} closed or no parking spaces.",
                    308,
                )
        except sqlalchemy.exc.IntegrityError:
            return "UNIQUE constraint failed", 400

    @app.route("/client_parkings/", methods=["DELETE"])
    def client_leaving_handler() -> tuple[str, int]:
        """Выезд клиента из парковочной зоны"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)
        client_check: Client = db.session.query(Client).get(client_id)

        if client_check.credit_card is True:
            c_parking: ClientParking = db.session.query(ClientParking).where(
                ClientParking.client_id == client_id
            )
            time_out = datetime.now()
            c_parking.time_out = time_out
            parking: Parking = db.session.query(Parking).get(parking_id)
            parking.count_available_places += 1
            db.session.commit()
            r_text = f"Client: {client_id} leaving parking zone: {parking_id}"
            return r_text, 308
        else:
            return f"Client: {client_id} cannot leave without payment", 308

    return app
